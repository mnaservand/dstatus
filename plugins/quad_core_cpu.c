/*
 * Copyright (c) 2012-2013, Patrick Steinhardt
 * Copyright (c) 2013, Hinnerk van Bruinehsen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <stdio.h>
#include <syslog.h>

#include "../config.h"

#define STAT "/proc/stat"
#define MAX(x, y) ((x) > (y) ? (x) : (y))

static FILE *statFd = 0;

void
consume_line(FILE *file)
{
	int ch = 1;
	while ((ch != '\n') && (ch != EOF))
		ch = fgetc(file);
}

bool
plugin_init()
{
    statFd = fopen(STAT,"r");

    return statFd != 0;
}

void
plugin_exit()
{
    fclose(statFd);
}

const char *
plugin_status()
{
    static char buf[QCPU_LEN];
    static unsigned long long lastUser[4], lastUserLow[4], lastSys[4], lastIdle[4];
    unsigned long long user[4], userLow[4], sys[4], idle[4];
    unsigned long long nonIdle[4], percent[4], total[4];

    if (statFd == 0) {
        return buf;
    }

    fflush(statFd);
    rewind(statFd);
    char str[] = "cpuI %Lu %Lu %Lu %Lu";
    char *scanFormat = malloc(sizeof(str));
    strcpy(scanFormat, str);

    char *err = "error";

    int i;
    for (i = 0; i < 4; i++) {
	    consume_line(statFd);
	    scanFormat[3] = '0' + i;

	    if (fscanf(statFd, scanFormat, &user[i], &userLow[i], &sys[i], &idle[i]) != 4) {
		syslog(LOG_ERR, "error reading CPU status\n");
		statFd = freopen(STAT, "r", statFd);
		return err;
	    } else {
		nonIdle[i] = (user[i] - lastUser[i]) + (userLow[i] - lastUserLow[i]) + (sys[i] - lastSys[i]);
		total[i] = MAX(nonIdle[i] + idle[i] - lastIdle[i], 1);

		/* Real usage percent */
		percent[i] = (nonIdle[i] * 100) / total[i];

		lastUser[i] = user[i];
		lastUserLow[i] = userLow[i];
		lastSys[i] = sys[i];
		lastIdle[i] = idle[i];
	    }

    }

    free(scanFormat);
    snprintf(buf, QCPU_LEN, QCPU_FORMAT, percent[0], percent[1], percent[2], percent[3]);
    return buf;
}

char
plugin_format()
{
    return 'q';
}
