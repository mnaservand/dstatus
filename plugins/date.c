/*
 * Copyright (c) 2013, Patrick Steinhardt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdbool.h>
#include <stdio.h>
#include <syslog.h>
#include <time.h>

#include "../config.h"

bool
plugin_init()
{
    return true;
}

void
plugin_exit()
{
    /* do nothing */
}

const char *
plugin_status()
{
    static char buf[DATE_LEN];
    static unsigned short cycle = 0;

    static time_t timer;
    static struct tm time_st;

    if (cycle++ % 60 != 0) {
        return buf;
    }

    timer = time(NULL);
    void *result = localtime_r(&timer, &time_st);
    if (result == NULL) {
        syslog(LOG_ERR, "error getting date\n");
    } else {
        snprintf(buf, DATE_LEN, "%d/%d/%d", time_st.tm_year + 1900, time_st.tm_mon + 1, time_st.tm_mday);
    }

    return buf;
}

char
plugin_format()
{
    return 'd';
}
