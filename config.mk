# paths
PREFIX = ${HOME}/.local/
SOPATH = ${PREFIX}/share/dstatus/

# plugins
PLUGINS = battery brightness cpu quad_core_cpu ip memory ram mpd selinux time volume wifi date jdate

# includes and libs
LIBS  = -lX11 -ldl
FLAGS = -DSOPATH=\"${SOPATH}\"

# flags
CFLAGS   = -Wall -Wextra -O3 -march=native ${FLAGS}
LDFLAGS  = ${LIBS}

# compiler and linker
CC = cc
